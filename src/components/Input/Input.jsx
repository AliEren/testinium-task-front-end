import { useState, useEffect } from "react";

function Input(props) {
    const {
        inputId,
        inputClass,
        inputRef,
        clearState
    } = props;

    const [value, setValue] = useState("");

    const handleOnChange = (value) => {
        setValue(value);
        inputRef.current = value;
    }

    useEffect(() => {
        handleOnChange("");
    }, [clearState]);

    return (
        <input
            id={`${inputId}`}
            className={`input ${inputClass}`}
            type="text"
            value={value}
            onChange={(event) => {
                handleOnChange(event.target.value)
            }}
            placeholder="Type your task"
        />
    );
}

export default Input;