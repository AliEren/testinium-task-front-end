function Button(props) {
    const {
        buttonId,
        buttonClass,
        textClass,
        text = "",
        imgSrc = "",
        imgClass,
        onClickAction
    } = props;

    return (
        <button id={buttonId} className={`button ${buttonClass}`} onClick={onClickAction}>
            {
                text !== "" && (
                    <span className={`text ${textClass}`}>
                        {text}
                    </span>
                )
            }

            {
                imgSrc !== "" && (
                    <img src={imgSrc} className={imgClass} />
                )
            }
        </button>
    );
}

export default Button;