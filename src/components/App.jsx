import { useState, useEffect, useRef } from "react";
import Input from "/src/components/Input/Input";
import Button from "/src/components/Button/Button";
import Task from "/src/components/Task/Task";
import axios from 'axios';

const BASE_URL = "http://localhost:8080";

function App() {
    const [tasksList, setTasksList] = useState([]);
    const [inputValue, setInputValue] = useState("");
    const [taskSubmitted, setTaskSubmitted] = useState("");
    const inputRef = useRef("");

    const addTask = async () => {
        try {
            const response = await axios({
                method: "post",
                headers: { "Content-Type": "application/json" },
                url: `${BASE_URL}/add-task`,
                data: JSON.stringify({
                    "value": inputRef.current
                })
            });

            if (response.status === 200) {
                setTaskSubmitted(!taskSubmitted);
                getAllTasks();
                // setInputValue("");
            } else {
                console.error("Error Code: ", response.status, "\nError occured at addTask function.");
            }
        } catch (err) {
            console.error("Error occured at addTask function: ", err);
        }
    }

    const deleteToDo = async (toDoId) => {
        try {
            const response = await axios({
                method: "post",
                headers: { "Content-Type": "application/json" },
                url: `${BASE_URL}/delete-task`,
                data: JSON.stringify({
                    "id": toDoId
                })
            });

            if (response.status === 200) {
                getAllTasks();
            } else {
                console.error("Error Code: ", response.status, "\nError occured at deleteToDo function.");
            }
        } catch (err) {
            console.error("Error occured at deleteToDo function: ", err);
        }
    }

    const getAllTasks = async () => {
        try {
            const response = await axios({
                method: 'get',
                url: `${BASE_URL}/get-all-tasks`
            });

            if (response.status === 200) {
                response.data.response != null ? setTasksList(response.data.response) : setTasksList([]);
            }
        } catch (err) {
            console.error("Error: ", err);
        }
    }

    useEffect(() => {
        getAllTasks();
    }, []);

    return (
        <div className="main">
            <div>
                <h1>Testinium To-Do App</h1>
                <hr className="heading-underline" />
            </div>

            <div className="c-add-task">
                <Input
                    inputId="task-input"
                    value={inputValue}
                    onChangeAction={setInputValue}
                    inputRef={inputRef}
                    clearState={taskSubmitted}
                />
                <Button
                    buttonClass="button"
                    text="Add Task"
                    buttonId="add-task-button"
                    onClickAction={addTask}
                />
            </div>

            <div className="c-task-list">
                {
                    tasksList && tasksList.map((task, index) => {
                        return (
                            <Task
                                key={`task-${index}`}
                                task={task}
                                onClickAction={() => deleteToDo(task.id)}
                            />
                        )
                    })
                }
            </div>
        </div>
    );
}

export { BASE_URL };
export default App;