import { useState } from "react";
import axios from "axios";
import { BASE_URL } from "../App";

import Button from "/src/components/Button/Button";
import trashCanIcon from "/src/assets/icons/trash-can.svg";

function Task(props) {
    const { task, onClickAction } = props;
    const [taskChecked, setTaskChecked] = useState(task.checked);

    const toggleCheck = async (checkedValue) => {
        try {
            const response = await axios({
                method: "post",
                headers: { "Content-Type": "application/json" },
                url: `${BASE_URL}/update-task`,
                data: JSON.stringify({
                    id: task.id,
                    value: task.value,
                    checked: !taskChecked
                })
            });

            if (response.status === 200) {
                response.data.response === 1 && setTaskChecked(!taskChecked);
            } else {
                console.error("Error Code: ", response.status, "\nError occured at toggleCheck function.");
            }

        } catch (err) {
            console.error("Error: ", err);
        }
    }

    return (
        <div className="c-task">
            <div
                className={`c-text ${taskChecked && "checked"}`}
                onClick={toggleCheck}
            >
                <p className="task-text">{task.value}</p>
            </div>

            <Button buttonClass="delete-button" imgSrc={trashCanIcon} imgClass="icon" onClickAction={onClickAction} />
        </div>
    );
}

export default Task;