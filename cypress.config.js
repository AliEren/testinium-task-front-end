import { defineConfig } from "cypress";

export default defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      // if (process.env.CI) {
      //   config.baseUrl = "https://staging.your-app.com";
      // } else {
      //   config.baseUrl = "http://localhost:3000";
      // }

      config.baseUrl = "http://localhost:3000";
      return config;
    },
  },

  component: {
    devServer: {
      framework: "react",
      bundler: "vite",
    },
  },
});
