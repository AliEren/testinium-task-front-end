# Testinium To-Do App Front-End
+ https://gitlab.com/AliEren/testinium-task-front-end

## Description
This project is created with Vite + ReactJS (node version: 20 LTS) and provides basic UI for users to perform CRUD operations on added To-Do tasks. In order to run the project just run "npm run dev" command after you install it. As a Version Control System I have chosen GitLab in order to benefit from the GitLab's pipeline. The project is deployed to the S3 service of the AWS. Both QA and Prod environment are hosted in S3 service.

## Structure
+ The "main.jsx" file is the starting point of the application and directs users to the "App.jsx". App component is the main component that covers entire application logic.
+ Vite normally runs the app at "localhost:5173" but I set it to the 3000.
### Components
Mainly there are just three components: Button, Input and Task. The Task component's functionality is to wrap the each added tasks.

## Acceptance Tests
As a test suite Cypress framework is being used in project. Acceptance tests can be found in directory of: "./cypress/e2e/acceptence-tests.cy.js". Acceptance Tests can be performed only if you leverage the back-end codes in your local. This is because missing deployment step of the back-end codes to a server.
### Custom Commands
Custom commands have been added by the suggestions of the official documentation and used in CI/CD pipeline.
+ "cy:open": "cypress open"
  - This command opens cypress in GUI version.
+ "e2e:chrome": "cypress run --browser chrome"
  - I choose Google Chrome engine in the CI/CD pipeline for debugging purposes.

## Pipeline
Despite I have failed to accomplish the project, just delivered the front-end in to AWS S3 buckets. I have originally 4 stages in pipeline but only the 3 is used. This is because I do not have a published back-end and tests needs a back-end to communicate. In some steps I kept some necessary variables in projets GitLab configuration specifically "Variables" section. By doing this GitLab could be get credentials and use it in pipeline.
+ QA: http://testinium-task-front-end-s3-119282350-qa.s3-website.eu-central-1.amazonaws.com
+ Prod: http://testinium-task-front-end-s3-119282350.s3-website.eu-central-1.amazonaws.com

### 1. Build Stage
+ image: In this stage as a docker image I go with one of the smallest Linux distros (alpine) available.
+ script: I simply run install and build the project in this step.
+ artifacts: The built is saved to GitLab artifacts for future operations.
### 2. Test Stage
+ image: Cypress Google Chrome image for test purposes.
+ script: As a first step, I clean install the project dependencies. Then run the program and tests in headless mode respectively.
### 3.Deploy To QA
+ environment: Specifies the environment to the GitLab automation.
+ extends: In order to separate the common codes those are used in both prod and qa, I added this line.
### 4.Deploy To Prod
Same configuration with the "3.Deploy To QA" step.
### 5. Deploy Common
+ image: In order to use the S3 AWS service, I needed this docker image.
+ rules: Just checks whether the default branch and the branch we push our codes are the same or not.
+ script: Synchronizes the dist (build) file currently built and dist file with the server version and deletes if there are any file that is not exists in dist file.

## Dockerfile
Despite I could not use custom docker file, I just created one. As a node version I choose the version that matches the project version. The application is kept in "/app" directory. I everything except the "node_modules" to the "/app" directory, installed and get build. As a last step I defined the port as 3000 and "npm run preview" to be able to run the build.

## Libraries Used In This Project
+ Cypress
+ Axios