describe('Given Empty ToDo list', () => {
    it('should add "buy some milk" to the to-do list', () => {
        cy.visit('/')

        // Add "buy some milk" task.
        cy.get("#task-input").type("buy some milk")
        cy.get("#add-task-button").click()

        // Check the added text value.
        cy.get('.task-text').last().should("have.text", "buy some milk")
        // Clear database.
        cy.get(".delete-button").last().click()
    })
})

describe('Given ToDo list with "buy some milk" item', () => {
    it('should append "enjoy the assignment" to the to-do list', () => {
        cy.visit('/')

        // Add "buy some milk" task.
        cy.get("#task-input").type("buy some milk")
        cy.get("#add-task-button").click()
        // Check the added text value.
        cy.get('.task-text').last().should("have.text", "buy some milk")
        cy.get(".delete-button").last().as("deleteButton_1")

        // Add "enjoy the assignment" task.
        cy.get("#task-input").type("enjoy the assignment")
        cy.get("#add-task-button").click()
        // Check the added text value.
        cy.get('.task-text').last().should("have.text", "enjoy the assignment")
        cy.get(".delete-button").last().as("deleteButton_2")

        // Clear database.
        cy.get("@deleteButton_1").click()
        cy.wait(500);
        cy.get("@deleteButton_2").click()
    })
})

describe('Given ToDo list with "buy some milk" item', () => {
    it('"buy some milk" should be marked when i click on it', () => {
        cy.visit('/')

        // Add "buy some milk" task.
        cy.get("#task-input").type("buy some milk")
        cy.get("#add-task-button").click()
        cy.get(".task-text").last().should("have.text", "buy some milk")
    })
})

describe('Given ToDo list with marked item', () => {
    it('"buy some milk" should be unmarked when i click on it', () => {
        cy.visit('/')

        cy.get(".task-text").last().click()
        cy.get(".c-text").should("not.have.class", "checked")

        // Clear database.
        cy.get(".delete-button").last().click()
    })
})

describe('Given ToDo list with "rest for a while" item', () => {
    it('should delete "rest for a while" task', () => {
        cy.visit('/')

        // Add "rest for a while" task.
        cy.get("#task-input").type("rest for a while")
        cy.get("#add-task-button").click()
        cy.get(".task-text").last().should("have.text", "rest for a while")

        // Delete added task.
        cy.get(".delete-button").last().click()
        cy.get(".c-task").should("not.exist")
    })
})

describe('Given ToDo list with "rest for a while" and "drink water" item', () => {
    it('should delete "rest for a while"', () => {
        cy.visit('/')

        cy.get("#task-input").type("rest for a while")
        cy.get("#add-task-button").click()
        cy.get('.task-text').last().should("have.text", "rest for a while")
        cy.get(".delete-button").last().as("deleteButton_1")

        cy.get("#task-input").type("drink water")
        cy.get("#add-task-button").click()
        cy.get('.task-text').last().should("have.text", "drink water")
        cy.get(".delete-button").last().as("deleteButton_2")

        // Clear database.
        cy.get("@deleteButton_1").click()
        cy.wait(500);
        cy.get("@deleteButton_2").click()

        // cy.get(".c-task").last().should("not.exist")
    })
})